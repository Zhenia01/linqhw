﻿using System;
using LINQ_HW.BL.Services;
using LINQ_HW.DAL.Models;
using LINQ_HW.DAL.Services;
using Task = System.Threading.Tasks.Task;

namespace LINQ_HW.I
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var collections = await JoinedCollections.Create(new ApiService());
            Queries queries = new Queries(collections);

            Console.WriteLine("1. Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків):");
            Console.WriteLine("    Для користувача з id = 1:");
            foreach ((Project p, int count) in queries.TasksPerProjectCount(1))
            {
                Console.WriteLine($"\t Проект {p.Id} {p.Name}: {count}");
            }

            Console.WriteLine("2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків)");
            Console.WriteLine("    Для користувача з id = 1:");
            Console.WriteLine("\t Таски:");
            foreach (var task in queries.TasksWithNamesUpTo45(1))
            {
                Console.WriteLine($"\t {task.Id} {task.Name}");
            }
            
            Console.WriteLine("3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2020) році для конкретного користувача (по id).");
            Console.WriteLine("    Для користувача з id = 1:");
            Console.WriteLine("\t Таски:");
            foreach (var task in queries.TasksFinishedThisYear(1))
            {
                Console.WriteLine($"\t {task.id} {task.name}");
            }

            Console.WriteLine("4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.");
            foreach (var tuple in queries.SortedTeamsWithAllMembersOlderThan10())
            {
                Console.WriteLine($"\t Команда {tuple.id} {tuple.name}:");
                foreach (var member in tuple.members)
                {
                    Console.WriteLine($"\t\t {member.Id} {member.FirstName} {member.LastName}");
                }
            }

            Console.WriteLine("5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks по довжині name (за спаданням).");
            foreach (var usersAndTask in queries.SortedUsersAndTasks())
            {
                Console.WriteLine($"\t Користувач: {usersAndTask.user.Id} {usersAndTask.user.FirstName} {usersAndTask.user.LastName}:");
                foreach (var task in usersAndTask.tasks)
                {
                    Console.WriteLine($"\t\t {task.Id} {task.Name}");
                }
            }

            Console.WriteLine("6. Отримати наступну структуру (передати Id користувача в параметри)....");
            Console.WriteLine("    Для користувача з id = 1:");
            var userReport = queries.UserReport(1);
            Console.WriteLine($"\t Користувач: {userReport.user.Id} {userReport.user.FirstName} {userReport.user.LastName}");
            Console.WriteLine($"\t Останній проект користувача (за датою створення): {userReport.lastProject.Id} {userReport.lastProject.Name}");
            Console.WriteLine($"\t Загальна кількість тасків під останнім проектом: {userReport.lastProjectTaskCount}");
            Console.WriteLine($"\t Загальна кількість незавершених або скасованих тасків для користувача: {userReport.uncompletedTaskCount}");
            Console.WriteLine($"\t Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений: {userReport.longestDateTask.Id} {userReport.longestDateTask.Name}");

            Console.WriteLine("7. Отримати таку структуру:");
            var projectsReport = queries.ProjectReports();
            
            
            Console.ReadKey();
        }
    }
}