using System;
using System.Collections.Generic;
using System.Linq;
using LINQ_HW.DAL.Models;
using Task = LINQ_HW.DAL.Models.Task;

namespace LINQ_HW.BL.Services
{
    public class Queries
    {
        private readonly JoinedCollections _collections;

        public Queries(JoinedCollections collections)
        {
            _collections = collections;
        }

        public IDictionary<Project, int> TasksPerProjectCount(int userId)
        {
            return _collections.Projects
                .Where(p => p.AuthorId == userId)
                .ToDictionary(p => p, p => p.Tasks.Count());
        }

        public IEnumerable<Task> TasksWithNamesUpTo45(int userId)
        {
            return _collections.Tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45);
        }

        private readonly int _currentYear = DateTime.Now.Year;

        public IEnumerable<(int id, string name)> TasksFinishedThisYear(int userId)
        {
            return _collections.Tasks.Where(t => t.PerformerId == userId && t.FinishedAt.Year == _currentYear)
                .Select(t => (t.Id, t.Name));
        }

        public IEnumerable<(int id, string name, IEnumerable<User> members)> SortedTeamsWithAllMembersOlderThan10()
        {
            bool IsOlderThan10(int birthYear) => _currentYear - birthYear >= 11;

            return _collections.Teams.Where(t => t.Members.All(m => IsOlderThan10(m.Birthday.Year)))
                .Select(t => (t.Id, t.Name, t.Members.OrderByDescending(m => m.RegisteredAt).AsEnumerable()));
        }

        public IEnumerable<(User user, IEnumerable<Task> tasks)> SortedUsersAndTasks()
        {
            return _collections.Users.OrderBy(u => u.FirstName)
                .Select(
                    u => (u, _collections.Tasks.Where(t => t.PerformerId == u.Id).OrderByDescending(t => t.Name.Length)
                        .AsEnumerable()));
        }

        public (User user, Project lastProject, int lastProjectTaskCount, int uncompletedTaskCount, Task longestDateTask
            )
            UserReport(int userId)
        {
            var lastProject = _collections.Projects.Where(p => p.AuthorId == userId).OrderBy(p => p.CreatedAt)
                .LastOrDefault();

            var userTasks = _collections.Tasks.Where(t => t.PerformerId == userId);

            return (
                _collections.User(userId),
                lastProject, lastProject?.Tasks?.Count() ?? 0,
                userTasks.Count(t => t.TaskState.Value != "Finished"),
                userTasks.OrderBy(t => t.CreatedAt).FirstOrDefault());
        }

        public IEnumerable<(Project project, Task longestDescriptionTask, Task shortestNameTask, int userCount)>
            ProjectReports()
        {
            return _collections.Projects.Where(p => p.Description.Length > 20 || p.Tasks.Count() < 3).Select(p =>
                (p, p.Tasks.OrderBy(t => t.Description).LastOrDefault(), p.Tasks.OrderBy(t => t.Name).FirstOrDefault(),
                    p.Team.Members.Count()));
        }
    }
}