using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LINQ_HW.DAL.Models;
using LINQ_HW.DAL.Services;
using Task = LINQ_HW.DAL.Models.Task;

namespace LINQ_HW.BL.Services
{
    public sealed class JoinedCollections
    {
        public IEnumerable<User> Users { get; private set; }
        public User User(int id) => Users.FirstOrDefault(u => u.Id == id);

        public IEnumerable<Team> Teams { get; private set; }
        public Team Team(int id) => Teams.FirstOrDefault(t => t.Id == id);

        public IEnumerable<Task> Tasks { get; private set; }
        public Task Task(int id) => Tasks.FirstOrDefault(t => t.Id == id);

        public IEnumerable<Project> Projects { get; private set; }
        public Project Project(int id) => Projects.FirstOrDefault(p => p.Id == id);
        
        public IEnumerable<TaskState> TaskStates { get; private set; }
        public TaskState TaskState(int id) => TaskStates.FirstOrDefault(p => p.Id == id);

        private JoinedCollections()
        {
        }

        private async Task<JoinedCollections> Initialize(ApiService service)
        {
            Users = await service.GetUsers();
            Teams = await service.GetTeams();
            Projects = await service.GetProjects();
            Tasks = await service.GetTasks();
            TaskStates = await service.GetTaskStates();

            // populate team with members and user with a team
            Teams = Teams.GroupJoin(
                Users,
                t => t.Id,
                u => u.TeamId,
                (team, users) =>
                {
                    team.Members = Users;

                    foreach (var user in users)
                    {
                        user.Team = team;
                    }

                    return team;
                });

            // populate project with an author
            Projects = Projects.Join(
                Users,
                p => p.AuthorId,
                u => u.Id,
                (project, user) =>
                {
                    project.Author = user;
                    return project;
                });

            // populate project with a team
            Projects = Projects.Join(
                Teams,
                p => p.AuthorId,
                t => t.Id,
                (project, team) =>
                {
                    project.Team = team;
                    return project;
                });

            // populate project with tasks and task with a project
            Projects = Projects.GroupJoin(
                Tasks,
                p => p.Id,
                t => t.ProjectId,
                (project, tasks) =>
                {
                    project.Tasks = tasks;

                    foreach (var task in tasks)
                    {
                        task.Project = project;
                    }

                    return project;
                });

            // populate task with a performer
            Tasks = Tasks.Join(
                Users,
                t => t.PerformerId,
                u => u.Id,
                (task, user) =>
                {
                    task.Performer = user;
                    return task;
                });
            
            // populate task with a state
            Tasks = Tasks.Join(
                TaskStates,
                t => t.State,
                ts => ts.Id,
                (task, taskState) =>
                {
                    task.TaskState = taskState;
                    return task;
                });
            
            return this;
        }

        public static Task<JoinedCollections> Create(ApiService service)
        {
            var ser = new JoinedCollections();
            return ser.Initialize(service);
        }
    }
}