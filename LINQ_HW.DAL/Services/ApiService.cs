using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using LINQ_HW.DAL.Config;
using LINQ_HW.DAL.Interfaces;
using LINQ_HW.DAL.Models;

using Task = LINQ_HW.DAL.Models.Task;

namespace LINQ_HW.DAL.Services
{
    public class ApiService : IDataAccessService
    {
        private static readonly HttpClient HttpClient;

        static ApiService()
        {
            HttpClient = new HttpClient();
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<User>>(ApiEndpoints.Users.GetUsers))
                    .ToImmutableList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<User>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<User>();
            }
        }

        public async Task<User> GetUser(int id)
        {
            try
            {
                return
                    await HttpClient.GetFromJsonAsync<User>(ApiEndpoints.Users.GetUser(id));
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return null;
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return null;
            }
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<Team>>(ApiEndpoints.Teams.GetTeams))
                    .ToImmutableList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<Team>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<Team>();
            }
        }

        public async Task<Team> GetTeam(int id)
        {
            try
            {
                return
                    await HttpClient.GetFromJsonAsync<Team>(ApiEndpoints.Teams.GetTeam(id));
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return null;
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return null;
            }
        }

        public async Task<IEnumerable<Task>> GetTasks()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<Task>>(ApiEndpoints.Tasks.GetTasks))
                    .ToImmutableList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<Task>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<Task>();
            }
        }

        public async Task<Task> GetTask(int id)
        {
            try
            {
                return
                    await HttpClient.GetFromJsonAsync<Task>(ApiEndpoints.Tasks.GetTask(id));
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return null;
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return null;
            }
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<Project>>(ApiEndpoints.Projects.GetProjects))
                    .ToImmutableList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<Project>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<Project>();
            }
        }

        public async Task<Project> GetProject(int id)
        {
            try
            {
                return
                    await HttpClient.GetFromJsonAsync<Project>(ApiEndpoints.Projects.GetProject(id));
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return null;
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return null;
            }
        }

        public async Task<IEnumerable<TaskState>> GetTaskStates()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<TaskState>>(ApiEndpoints.TaskStates.GetTaskStates))
                    .ToImmutableList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<TaskState>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<TaskState>();
            }
        }
    }
}