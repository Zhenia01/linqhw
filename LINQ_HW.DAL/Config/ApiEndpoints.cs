namespace LINQ_HW.DAL.Config
{
    public static class ApiEndpoints
    {
        private const string BaseUrl = "https://bsa20.azurewebsites.net/api";

        public static class Users
        {
            private const string Category = "Users";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetUsers = $"{Route}";
            public static string GetUser(int id) => $"{Route}/{id}";
        }
        
        public static class Teams
        {
            private const string Category = "Teams";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetTeams = $"{Route}";
            public static string GetTeam(int id) => $"{Route}/{id}";
        }
        
        public static class Projects
        {
            private const string Category = "Projects";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetProjects = $"{Route}";
            public static string GetProject(int id) => $"{Route}/{id}";
        }
        
        public static class Tasks
        {
            private const string Category = "Tasks";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetTasks = $"{Route}";
            public static string GetTask(int id) => $"{Route}/{id}";
        }
        
        public static class TaskStates
        {
            private const string Category = "TaskStates";

            private static readonly string Route = $"{BaseUrl}/{Category}";

            public static readonly string GetTaskStates = $"{Route}";
        }
    }
}