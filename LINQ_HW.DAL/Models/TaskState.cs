namespace LINQ_HW.DAL.Models
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}