using System;
using System.Collections.Generic;

namespace LINQ_HW.DAL.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public IEnumerable<User> Members { get; set; }
    }
}