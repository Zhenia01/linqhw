using System.Collections.Generic;
using System.Threading.Tasks;
using LINQ_HW.DAL.Models;

using Task = LINQ_HW.DAL.Models.Task;

namespace LINQ_HW.DAL.Interfaces
{
    public interface IDataAccessService
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(int id);
        
        Task<IEnumerable<Team>> GetTeams();
        Task<Team> GetTeam(int id);
        
        Task<IEnumerable<Task>> GetTasks();
        Task<Task> GetTask(int id);
        
        Task<IEnumerable<Project>> GetProjects();
        Task<Project> GetProject(int id);
        
        Task<IEnumerable<TaskState>> GetTaskStates();
    }
}